import React from 'react';
import ReactDOM from 'react-dom';
import { faker } from '@faker-js/faker';
import Commentdetail from './Commentdetail';
import ApprovalCard from './ApprovalCard';

const App = ()=>{
    return(
       <div className='ui container comments'>
        
        <ApprovalCard>
            <Commentdetail 
            author='pankaj'
            timeAgo='today at 2:00 am' 
            content='Ram Ram' 
            avatar={faker.image.avatar()}
            />
         </ApprovalCard>

         <ApprovalCard>
            <Commentdetail 
            author='Neeraj'
            timeAgo='today at 5:00 am'
            content='Good Work'
            avatar={faker.image.avatar()}
            />
        </ApprovalCard>

        <ApprovalCard>
            <Commentdetail 
            author='Aman' 
            timeAgo='yesterday at 6:00 am' 
            content='Good Morning' 
            avatar={faker.image.avatar()}
            />
        </ApprovalCard>

       </div>
    );
}

ReactDOM.render(<App/> , document.getElementById("root"))